//
//  ImagesTableViewController.m
//  ImageDownloader
//
//  Created by dor on 20.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "ImagesTableViewController.h"


@interface ImagesTableViewController ()

@property (strong, nonatomic) NSMutableArray *sectionBuffer;
@property (strong, nonatomic) NSMutableArray *tvSections;
@property (strong, nonatomic) NSMutableArray *images;
@property NSInteger sectionsCount;

@end

@implementation ImagesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sectionsCount = 1;
    self.images = [ NSMutableArray new ];
    self.tvSections = [ NSMutableArray new ];
    self.sectionBuffer = [ NSMutableArray new ];
    
    NSMutableArray *operations = [ NSMutableArray new ];
    
    for ( int i = 0 ; i < 9 ; i++ ) {
        [ operations addObject:[ ImageDownload new ] ];
    }
    
    NSOperationQueue *queue = [ NSOperationQueue new ];
    
    for ( ImageDownload *operation in operations ) {
        operation.imageURL = @"http://www.h3dwallpapers.com/wp-content/uploads/2014/08/Landscape-wallpapers-6.jpg";
        operation.delegate = self;
        
        [ queue addOperation:operation ];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return self.sectionsCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
    
    cell.frame = CGRectMake( 0, 0, cell.frame.size.width, 100 );
    
    UIImageView *thumb = [[ UIImageView alloc ] initWithFrame:CGRectMake(10, cell.frame.size.height*0.1, cell.frame.size.height*0.8, cell.frame.size.height*0.8 ) ];
    if ( self.images.count > 0 ) {
        if ( self.images.count > indexPath.row ) {
            thumb.image = [ self.images objectAtIndex:indexPath.row ];
        }
    }
    [ cell addSubview:thumb ];
    
    return cell;
}

- (void)didFinishedImageDownloading:(id)image {
    if ( self.images.count < self.sectionsCount*3 ) {
        [ self.images addObject:image ];
    } else {
        self.sectionsCount++;
        [ self.images addObject:image ];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [ self.tableView reloadData ];

    });
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,320,10)];
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    label.text = @"SEKCJA";
    
    return label;
}

@end
