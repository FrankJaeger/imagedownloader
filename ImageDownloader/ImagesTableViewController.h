//
//  ImagesTableViewController.h
//  ImageDownloader
//
//  Created by dor on 20.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageDownload.h"

@interface ImagesTableViewController : UITableViewController<ImageDownloadDelegate>

@end
