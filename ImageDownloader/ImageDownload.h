//
//  ImageDownload.h
//  ImageDownloader
//
//  Created by dor on 20.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PLH_URL

@protocol ImageDownloadDelegate <NSObject>

-(void)didFinishedImageDownloading:(id)image;

@end

@interface ImageDownload : NSOperation

@property (weak, nonatomic) id<ImageDownloadDelegate> delegate;

@property (weak, nonatomic) NSString *imageURL;


@end
