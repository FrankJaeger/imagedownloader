//
//  ImageDownload.m
//  ImageDownloader
//
//  Created by dor on 20.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "ImageDownload.h"
#import <UIKit/UIKit.h>



@implementation ImageDownload {
    BOOL executing;
    BOOL finished;
}

- (id)init {
    self = [ super init ];
    
    if ( self ) {
        executing = finished = NO;
    }

    return self;
}

- (void)start {
    __strong typeof (self.imageURL) strongURL = self.imageURL;
    [ self willChangeValueForKey:@"isExecuting" ];
        executing = YES;
    [ self didChangeValueForKey:@"isExecuting" ];
    
    if ( self.imageURL != nil ) {
        
        NSURLRequest *request = [ NSURLRequest requestWithURL:[NSURL URLWithString:strongURL ] ];
        
        NSError *error;
        NSURLResponse *response;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if ( error == nil && data) {
            if ( [ self.delegate respondsToSelector:@selector(didFinishedImageDownloading:) ] ) {
                [ self.delegate didFinishedImageDownloading:[ UIImage imageWithData:data ] ];
            }
        }
    }
    
    [ self willChangeValueForKey:@"isExecuting" ];
    [ self willChangeValueForKey:@"isFinished" ];
    finished = YES;
    executing = NO;
    [ self didChangeValueForKey:@"isExecuting" ];
    [ self didChangeValueForKey:@"isFinished" ];
    
}







#pragma mark Operation Delegate

- (BOOL)isExecuting {
    
    return executing;
}

- (BOOL)isFinished {
    
    return finished;
}

- (BOOL)isConcurrent {
    
    return YES;
}

@end
